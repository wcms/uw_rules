<?php

/**
 * @file
 * UWaterloo Rules code: conditions and events.
 */

/**
 * Implements hook_rules_condition_info().
 * Declares any meta-data about conditions for Rules.
 */
function uw_rules_rules_condition_info() {

  $conditions = array();

  if (module_exists('scanner')) {
    $conditions['uw_rules_condition_enable_scanner'] = array(
      'label' => t('Enable scanner rules'),
      'group' => t('uWaterloo Rules'),
    );

    $conditions['uw_rules_condition_enable_scanner_redo'] = array(
      'label' => t('Enable scanner redo rules'),
      'group' => t('uWaterloo Rules'),
    );
  }

  return $conditions;
}

/**
 *
 */
function uw_rules_condition_enable_scanner() {
  return ((variable_get('uw_scanner_rule_config_rules_enable') == 1)
      && ((current_path() == 'admin/content/scanner') ? TRUE : FALSE));
}

/**
 *
 */
function uw_rules_condition_enable_scanner_redo() {

  if ((current_path() == 'admin/content/scanner/undo/confirm') || (current_path() == 'admin/content/scanner/undo')) {
    return ((variable_get('uw_scanner_rule_config_rules_enable') == 1) ? TRUE : FALSE);
  }
}

/**
 * Implements hook_rules_event_info().
 * Declares any meta-data about events for Rules.
 */
function uw_rules_rules_event_info() {
  $events = array();

  if (module_exists('scanner')) {

    $events['uw_rules_event_revision_update'] = array(
      'label' => t('Content revision has been updated'),
      'group' => t('uWaterloo Rules'),
      'variables' => _uw_rules_rules_event_variables(),
    );
  }
  return $events;
}

/**
 *
 */
function _uw_rules_rules_event_variables() {
  $vars = array(
    'node' => array(
      'type' => 'node',
      'label' => t('target node.'),
    ),
    'revision' => array(
      'type' => 'node',
      'label' => t('current revision of target content.'),
      'description' => t('The current content revision'),
      'handler' => 'uw_rules_event_argument_current_revision',
    ),
  );
  return $vars;
}

/**
 * Evaluate revision argument.
 */
function uw_rules_event_argument_current_revision($arguments, $name, $info) {
  if (empty($arguments['node'])) {
    drupal_set_message(t('Revisioning: could not evaluate rule condition -- node variable missing.'), 'warning');
    return FALSE;
  }
  $node = $arguments['node'];
  $current_vid = $node->current_revision_id;
  if ($node->vid != $current_vid) {
    $current = node_load($node->nid, $current_vid);
    return $current;
  }
  return $node;
}
